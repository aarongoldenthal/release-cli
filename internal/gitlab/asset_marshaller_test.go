package gitlab

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAssetMarshaller(t *testing.T) {
	tests := map[string]struct {
		name                  string
		assetLink             []string
		expectedAssetLength   int
		expectedErrorMessage  string
		expectedOriginalError string
		expectedAssets        *Assets
	}{
		"nil": {
			assetLink:           nil,
			expectedAssetLength: 0,
		},
		"empty": {
			assetLink:           []string{},
			expectedAssetLength: 0,
		},
		"empty_string": {
			assetLink:            []string{""},
			expectedAssetLength:  0,
			expectedErrorMessage: `invalid delimiter for asset: ""`,
		},
		"one_asset": {
			assetLink:           []string{`{"name":"asset1","url":"https://<domain>/some/location/1","link_type":"other","direct_asset_path":"xzy1"}`},
			expectedAssetLength: 1,
			expectedAssets: &Assets{Links: []*Link{
				{Name: "asset1", URL: "https://<domain>/some/location/1", LinkType: "other", DirectAssetPath: "xzy1"},
			}},
		},
		"one_asset_filepath_alias_to_direct_asset_path": {
			assetLink:           []string{`{"name":"asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`},
			expectedAssetLength: 1,
			expectedAssets: &Assets{Links: []*Link{
				{Name: "asset1", URL: "https://<domain>/some/location/1", LinkType: "other", DirectAssetPath: "xzy1"},
			}},
		},
		"one_asset_with_filepath_direct_asset_path_conflict": {
			assetLink:             []string{`{"name":"asset1","url":"https://<domain>/some/location/1","link_type":"other","direct_asset_path": "xyz2", "filepath":"xzy1"}`},
			expectedAssetLength:   0,
			expectedErrorMessage:  "asset link can either specify `direct_asset_path` or `filepath` (`filepath` is deprecated).",
			expectedOriginalError:  "asset link can either specify `direct_asset_path` or `filepath` (`filepath` is deprecated).",
		},
		"one_asset_with_whitespaces": {
			assetLink:           []string{`    {"name":"asset1","url":"https://<domain>/some/location/1","link_type":"other","direct_asset_path":"xzy1"}`},
			expectedAssetLength: 1,
			expectedAssets: &Assets{Links: []*Link{
				{Name: "asset1", URL: "https://<domain>/some/location/1", LinkType: "other", DirectAssetPath: "xzy1"},
			}},
		},
		"one_malformed_string": {
			assetLink:             []string{`{"fake_asset}`},
			expectedAssetLength:   0,
			expectedErrorMessage:  `invalid asset: "{\"fake_asset}"`,
			expectedOriginalError: "unexpected end of JSON input",
		},
		"one_malformed_asset": {
			assetLink:             []string{`{"fake_asset"}`},
			expectedAssetLength:   0,
			expectedErrorMessage:  `invalid asset: "{\"fake_asset\"}"`,
			expectedOriginalError: "after object key",
		},
		"invalid_delimiter": {
			assetLink:            []string{`\t{"name":"asset1"}`},
			expectedAssetLength:  0,
			expectedErrorMessage: `invalid delimiter for asset: "\\t{\"name\":\"asset1\"}"`,
		},
		"couple_of_single_assets": {
			assetLink:           []string{`{"name":"asset2", "url":"https://gitlab.com/gitlab-org"}`, `{"name":"asset3", "url":"https://gitlab.com"}`},
			expectedAssetLength: 2,
			expectedAssets: &Assets{Links: []*Link{
				{Name: "asset2", URL: "https://gitlab.com/gitlab-org"},
				{Name: "asset3", URL: "https://gitlab.com"},
			}},
		},
		"valid_asset_with_an_invalid_one": {
			assetLink:             []string{`{"name":"asset1","url":"https://<domain>/some/location/1"}, {"invalid_asset"}`},
			expectedAssetLength:   0,
			expectedErrorMessage:  `invalid asset: "{\"name\":\"asset1\",\"url\":\"https://<domain>/some/location/1\"}, {\"invalid_asset\"}"`,
			expectedOriginalError: "invalid character",
		},
		"array_of_assets": {
			assetLink:           []string{`[{"name":"asset2", "url":"https://gitlab.com/gitlab-org"}, {"name":"asset3", "url":"https://gitlab.com/gitlab-org/gitlab"}]`},
			expectedAssetLength: 2,
			expectedAssets: &Assets{Links: []*Link{
				{Name: "asset2", URL: "https://gitlab.com/gitlab-org"},
				{Name: "asset3", URL: "https://gitlab.com/gitlab-org/gitlab"},
			}},
		},
		"malformed_array_of_assets": {
			assetLink:             []string{`["fake_asset", "another_fake_asset"]`},
			expectedAssetLength:   0,
			expectedErrorMessage:  `invalid array of assets: "[\"fake_asset\", \"another_fake_asset\"]"`,
			expectedOriginalError: "json: cannot unmarshal string into Go value of type gitlab.Link",
		},
		"single_and_array_of_assets": {
			assetLink:           []string{`{"name":"asset1", "url":"https://gitlab.com"}`, `[{"name":"asset2", "url":"https://gitlab.com/gitlab-org"}, {"name":"asset3", "url":"https://gitlab.com/gitlab-org/gitlab"}]`, `{"name":"asset4", "url":"https://gitlab.com/gitlab-org/release-cli"}`},
			expectedAssetLength: 4,
			expectedAssets: &Assets{Links: []*Link{
				{Name: "asset1", URL: "https://gitlab.com"},
				{Name: "asset2", URL: "https://gitlab.com/gitlab-org"},
				{Name: "asset3", URL: "https://gitlab.com/gitlab-org/gitlab"},
				{Name: "asset4", URL: "https://gitlab.com/gitlab-org/release-cli"},
			}},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			assets, err := AssetMarshaller(tt.assetLink)

			if tt.expectedErrorMessage != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.expectedErrorMessage)
				if tt.expectedOriginalError != "" {
					require.Contains(t, err.Error(), tt.expectedOriginalError)
				}
				return
			}

			require.NoError(t, err)
			require.NotNil(t, assets)
			require.Len(t, assets.Links, tt.expectedAssetLength)

			if tt.expectedAssets != nil {
				require.EqualValues(t, assets, tt.expectedAssets)
			}
		})
	}
}
