module gitlab.com/gitlab-org/release-cli

go 1.19

require (
	github.com/cyphar/filepath-securejoin v0.2.4
	github.com/hashicorp/go-multierror v1.1.1
	github.com/jstemmer/go-junit-report v1.0.0
	github.com/mitchellh/gox v1.0.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.4
	github.com/urfave/cli/v2 v2.4.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-version v1.4.0 // indirect
	github.com/mitchellh/iochan v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/objx v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
